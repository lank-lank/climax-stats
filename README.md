# Application de rapports statistiques sur les clients (ClimXstats)

----

### Objectif du Projet
Créer une application destinée à produire des rapports statistiques sur les clients, en traitant les données fournies par des partenaires dans divers formats tels que texte, CSV, XML, JSON, etc. L'objectif principal est de concevoir une application capable de lire et interpréter ces données provenant de différents fichiers, ainsi que d'autres formats éventuels.

### Fonctionnalités principales
1. **Lecture de divers formats de fichiers**
   - L'application est capable de traiter des fichiers au format TXT, CSV, XML, JSON, XLSX, XLS, YAML et peut être étendue pour supporter d'autres formats. Les modèles de fichier sont dans le dossier "resources".
   - La solution garantit une robustesse et une évolutivité pour faire face aux évolutions futures, en utilisant des design patterns tels que "Strategy" pour la mise en œuvre des différents lecteurs de fichiers, et "Factory" pour la création des lecteurs en fonction du type.

2. **Calcul de la moyenne des salaires par profession**
   - L'application propose une fonctionnalité permettant de calculer la moyenne des salaires en fonction du type de profession.

### Technologies et outils
* Système d'exploitation: Windows 10
* Langage de programmation : Java ([_voir..._](https://www.oracle.com/fr/java/))
* Gestion des dépendances : Maven ([_voir..._](https://maven.apache.org))
* Kit de développement : JDK 17 ([_télécharger
  ici..._](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html))
* IDE: IntelliJ Ultimate 2023.2 ([_télécharger..._](https://www.jetbrains.com/fr-fr/idea/))
* Framework d'interface utilisateur : Javafx 17 ou OpenJfx ([_voir..._](https://openjfx.io/))
* Logiciel de conception d'interface utilisateur: SceneBuilder 21 ([_télécharger
  ici..._](https://gluonhq.com/products/scene-builder/))
* Librairie de composants UI : JFoenix 9 ([_télécharger ici..._](https://jar-download.com/?search_box=jfoenix))
* Outils de versionning : Git ([_voir..._](https://git-scm.com/)) et la plateforme GitLab ([_voir..._](https://gitlab.com/))

### Prérequis
- Avoir Java 17 installé
- Téléchargez JavaFX 17 (([_télécharger ici..._](https://gluonhq.com/products/javafx/)) et extrayez-le dans un dossier distinct. La version 17 de Java n'inclus 
pas JavaFX dans le JDK, contrairement à Java 8. Ceci est nécessaire pour exécuter l'application.

### Utilisation et exécution
L'application est un fichier JAR généré à l'aide de la commande `mvn install`. Ce fichier JAR est [climaxstats-1.0-SNAPSHOT.jar](./climaxstats-1.0-SNAPSHOT.jar)
Pour lancer l'application, utilisez la commande suivante avec la version Java 17:

> `java --module-path "dossier_contenant_javafx/lib" --add-modules javafx.base,javafx.controls,javafx.fxml,javafx.graphics -jar "dossier_contenant_le_jar/climaxstats-1.0-SNAPSHOT.jar"`
