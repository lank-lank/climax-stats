# Utilisez l'image OpenJDK 17 comme base
FROM openjdk:17

# Définissez le répertoire de travail dans le conteneur
WORKDIR /test/agitex/

# Copiez le fichier JAR généré et le dossier contenant JavaFX
COPY ./climaxstats-1.0-SNAPSHOT.jar /test/agitex/app
COPY dossier_contenant_javafx/ /test/agitex/javafx

# Exécutez l'application avec la commande spécifiée
CMD ["java", "--module-path", "/test/agitex/javafx/lib", "--add-modules", "javafx.base,javafx.controls,javafx.fxml,javafx.graphics", "-jar", "/test/agitex/app/climaxstats-1.0-SNAPSHOT.jar"]
