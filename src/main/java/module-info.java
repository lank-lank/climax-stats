module bf.dav.nextep.climaxstats {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires javafx.base;
    requires com.jfoenix;
    requires com.google.common;
    requires org.apache.commons.io;
    requires org.apache.commons.lang3;
    requires log4j.api;
    requires univocity.parsers;
    requires xcelite;
    requires jakarta.xml.bind;
    requires jakarta.activation;
    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.annotation;
    requires com.fasterxml.jackson.dataformat.yaml;

    opens bf.dav.nextep.app to javafx.fxml, javafx.controls, javafx.graphics, javafx.base;
    opens bf.dav.nextep.app.controller to javafx.fxml, javafx.controls, javafx.graphics, javafx.base;
    opens bf.dav.nextep.app.notification to javafx.fxml, javafx.controls, javafx.graphics, javafx.base;
    opens bf.dav.nextep.app.notification.animation to javafx.fxml, javafx.controls, javafx.graphics, javafx.base;
    opens bf.dav.nextep.app.utils to javafx.fxml, javafx.controls, javafx.graphics, javafx.base;
    opens bf.dav.nextep.app.service to javafx.fxml, javafx.controls, javafx.graphics, javafx.base;
    opens bf.dav.nextep.app.service.factory to javafx.fxml, javafx.controls, javafx.graphics, javafx.base;
    opens bf.dav.nextep.app.service.task to javafx.fxml, javafx.controls, javafx.graphics, javafx.base;
    opens bf.dav.nextep.app.model to univocity.parsers, xcelite, javafx.controls, javafx.graphics, javafx.base;
    opens bf.dav.nextep.app.service.reader to jakarta.xml.bind, com.fasterxml.jackson.dataformat.yaml, jakarta.activation, com.fasterxml.jackson.core, com.fasterxml.jackson.databind, com.fasterxml.jackson.annotation;

    exports bf.dav.nextep.app;
    exports bf.dav.nextep.app.controller;
    exports bf.dav.nextep.app.notification;
    exports bf.dav.nextep.app.notification.animation;
    exports bf.dav.nextep.app.model;
    exports bf.dav.nextep.app.utils;
    exports bf.dav.nextep.app.service;
    exports bf.dav.nextep.app.service.task;
    exports bf.dav.nextep.app.service.reader;
    exports bf.dav.nextep.app.service.exception;
    exports bf.dav.nextep.app.service.factory;
}
