package bf.dav.nextep.app.model;

import com.ebay.xcelite.annotations.Column;
import com.univocity.parsers.annotations.Parsed;
import com.univocity.parsers.annotations.Trim;

public class Client {
    @Trim
    @Parsed(field = "Nom")
    @Column(name = "Nom")
    private String nom;
    @Trim
    @Parsed(field = "Prenom")
    @Column(name = "Prenom")
    private String prenom;
    @Trim
    @Parsed(field = "Age")
    @Column(name = "Age")
    private Integer age;
    @Trim
    @Parsed(field = "Profession")
    @Column(name = "Profession")
    private String profession;
    @Trim
    @Parsed(field = "Salaire")
    @Column(name = "Salaire", ignoreType = true)
    private Double salaire;

    public Client() {
    }

    public Client(String nom, String prenom,
                  Integer age, String profession,
                  Double salaire) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.profession = profession;
        this.salaire = salaire;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public Double getSalaire() {
        return salaire;
    }

    public void setSalaire(Double salaire) {
        this.salaire = salaire;
    }

}
