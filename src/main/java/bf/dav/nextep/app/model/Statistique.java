package bf.dav.nextep.app.model;

public class Statistique {
    private String profession;
    private Double salaireMoyen;

    public Statistique() {
    }

    public Statistique(String profession, Double salaireMoyen) {
        this.profession = profession;
        this.salaireMoyen = salaireMoyen;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public Double getSalaireMoyen() {
        return salaireMoyen;
    }

    public void setSalaireMoyen(Double salaireMoyen) {
        this.salaireMoyen = salaireMoyen;
    }
}
