package bf.dav.nextep.app.controller;

import bf.dav.nextep.app.ClimaxStatsApplication;
import bf.dav.nextep.app.model.Client;
import bf.dav.nextep.app.model.Statistique;
import bf.dav.nextep.app.service.FileReader;
import bf.dav.nextep.app.service.FileReaderContext;
import bf.dav.nextep.app.service.factory.FileReaderFactory;
import bf.dav.nextep.app.service.task.ReadService;
import bf.dav.nextep.app.utils.Toast;
import com.jfoenix.controls.JFXButton;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;
import java.util.stream.Collectors;

public class HomeController implements Initializable {
    @FXML
    private TextField file_browser;
    @FXML
    private JFXButton btn_lecture;
    @FXML
    private JFXButton btn_calculer;


    @FXML
    private TableView<Client> table_client;
    @FXML
    private TableColumn<Client, String> col_nom;
    @FXML
    private TableColumn<Client, String> col_prenom;
    @FXML
    private TableColumn<Client, String> col_age;
    @FXML
    private TableColumn<Client, String> col_profession;
    @FXML
    private TableColumn<Client, String> col_salaire;

    @FXML
    private TableView<Statistique> table_statistique;
    @FXML
    private TableColumn<Statistique, String> col_stat_profession;
    @FXML
    private TableColumn<Statistique, String> col_stat_salaire_moyen;

    private File fichierChoisi;
    private final ObservableList<Client> clients = FXCollections.observableList(new ArrayList<>());
    private final ObservableList<Statistique> statistiques = FXCollections.observableList(new ArrayList<>());


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        file_browser.setFocusTraversable(false);

        this.initializeTables();
    }

    private void initializeTables() {
        table_client.setItems(clients);
        col_nom.setCellValueFactory(cellData -> {
            String nom = cellData.getValue().getNom();
            return new SimpleStringProperty(Objects.requireNonNullElse(nom, "---"));
        });
        col_prenom.setCellValueFactory(cellData -> {
            String prenom = cellData.getValue().getPrenom();
            return new SimpleStringProperty(Objects.requireNonNullElse(prenom, "---"));
        });
        col_profession.setCellValueFactory(cellData -> {
            String profession = cellData.getValue().getProfession();
            return new SimpleStringProperty(Objects.requireNonNullElse(profession, "---"));
        });
        col_age.setCellValueFactory(cellData -> {
            Integer age = cellData.getValue().getAge();
            if (age == null) {
                return new SimpleStringProperty("---");
            }
            return new SimpleStringProperty(age.toString());
        });
        col_salaire.setCellValueFactory(cellData -> {
            Double salaire = cellData.getValue().getSalaire();
            if (salaire == null) {
                return new SimpleStringProperty("---");
            }
            return new SimpleStringProperty(formatter().format(salaire));
        });


        table_statistique.setItems(statistiques);
        col_stat_profession.setCellValueFactory(cellData -> {
            String profession = cellData.getValue().getProfession();
            return new SimpleStringProperty(Objects.requireNonNullElse(profession, "---"));
        });
        col_stat_salaire_moyen.setCellValueFactory(cellData -> {
            Double salaireMoyen = cellData.getValue().getSalaireMoyen();
            if (salaireMoyen == null) {
                return new SimpleStringProperty("---");
            }
            return new SimpleStringProperty(formatter().format(salaireMoyen));
        });
    }

    @FXML
    private void onChooseFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Selectionner le fichier");
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Excel/XML/CSV/TXT/JSON/YAML", "*.csv", "*.xlsx", "*.xls", "*.xml", "*.txt", "*.json", "*.yml")
        );
        File file = fileChooser.showOpenDialog(ClimaxStatsApplication.stage);

        if (file != null) {
            file_browser.setText(file.getPath());
            this.fichierChoisi = file;
        }
    }

    @FXML
    private void onReadFile() {
        if (this.fichierChoisi == null) {
            Toast.error("Fichier manquant", "Veuillez indiquez le fichier à lire !");
            return;
        }

        String extension = FilenameUtils.getExtension(fichierChoisi.getName());

        FileReader fileReader = FileReaderFactory.getFileReader(extension);
        if (fileReader == null) {
            String message = "Fichier non pris en charge !\nLe format de ce fichier n'est pas encore pris en charge";
            Toast.warning("Avertissement", message);
            return;
        }

        FileReaderContext context = new FileReaderContext(fileReader);
        ReadService service = new ReadService();
        service.setContext(context);
        service.setFile(fichierChoisi);

        service.restart();

        btn_lecture.disableProperty().bind(service.runningProperty());
        btn_calculer.disableProperty().bind(service.runningProperty());
        file_browser.disableProperty().bind(service.runningProperty());

        service.setOnSucceeded(event -> {
            file_browser.setText(null);
            this.fichierChoisi = null;

            List<Client> data = service.getValue();
            if (data != null) {
                if (data.isEmpty()) {
                    String message = "Contenu vide ! Aucune donnée n'a été lue";
                    Toast.warning("Avertissement", message);
                } else {
                    clients.addAll(data);
                    String message = String.format("Lecture des données du fichier\n%s ligne(s) lue(s)", data.size());
                    Toast.success("Lecture réussie", message);
                }
            } else {
                String message = "Lecture échouée\nUne erreur s'est produite";
                Toast.error("Echec", message);
            }
        });

        service.setOnFailed(event -> {
            Throwable exception = service.getException();
            if (exception != null) {
                String message = exception.getCause().getMessage();
                Toast.error(exception.getMessage(), message);
            } else {
                String message = "La lecture des a échoué\nUne erreur s'est produite";
                Toast.error("Echec de lecture", message);
            }
        });
    }


    private static DecimalFormat formatter() {
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setDecimalSeparator('.');
        decimalFormatSymbols.setGroupingSeparator(' ');

        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00", decimalFormatSymbols);
        decimalFormat.setParseBigDecimal(true);
        return decimalFormat;
    }

    @FXML
    public void onComputeAverage() {
        if (clients.isEmpty()) {
            String message = "Aucune donnée ! Veuillez lire d'abord les données";
            Toast.warning("Avertissement", message);
            return;
        }

        Map<String, DoubleSummaryStatistics> statsParProfession = clients
                .stream()
                .filter(client -> Objects.nonNull(client.getSalaire()))
                .collect(Collectors.groupingBy(
                        client -> {
                            String profession = client.getProfession();
                            profession = profession != null ? StringUtils.capitalize(client.getProfession().toLowerCase()) : "Autres";
                            return profession;
                        },
                        Collectors.summarizingDouble(Client::getSalaire)
                ));

        List<Statistique> salairesMoyens = statsParProfession.entrySet()
                .stream()
                .map(entry -> new Statistique(entry.getKey(), entry.getValue().getAverage()))
                .toList();

        statistiques.setAll(salairesMoyens);
    }
}
