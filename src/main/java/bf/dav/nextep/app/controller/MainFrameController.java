package bf.dav.nextep.app.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.StackPane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;


public class MainFrameController implements Initializable {
    @FXML
    public StackPane main_container;

    private static final Logger log = LogManager.getLogger(MainFrameController.class);


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loadView();
    }

    private void loadView() {
        String view = "home.fxml";
        try {
            URL url = Objects.requireNonNull(MainFrameController.class.getResource("/ui/" + view));
            StackPane pane = FXMLLoader.load(url);
            main_container.getChildren().setAll(pane);
        } catch (IOException e) {
            log.error(e);
        }
    }
}
