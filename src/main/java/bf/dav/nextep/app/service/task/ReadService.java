package bf.dav.nextep.app.service.task;

import bf.dav.nextep.app.model.Client;
import bf.dav.nextep.app.service.FileReaderContext;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

import java.io.File;
import java.util.List;

public class ReadService extends Service<List<Client>> {
    private FileReaderContext context;
    private File file;

    public void setContext(FileReaderContext context) {
        this.context = context;
    }

    public void setFile(File file) {
        this.file = file;
    }

    @Override
    protected Task<List<Client>> createTask() {
        return new ReadTask(context, file);
    }
}
