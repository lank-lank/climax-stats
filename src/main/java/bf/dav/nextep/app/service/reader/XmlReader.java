package bf.dav.nextep.app.service.reader;

import bf.dav.nextep.app.model.Client;
import bf.dav.nextep.app.service.FileReader;
import bf.dav.nextep.app.service.exception.FileReaderException;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class XmlReader implements FileReader {
    private static final Logger log = LogManager.getLogger(XmlReader.class);

    @Override
    public List<Client> getClients(File file) throws Exception {
        try {
            JAXBContext context = JAXBContext.newInstance(XmlNode.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            XmlNode node = (XmlNode) unmarshaller.unmarshal(file);

            return node.getClients();
        } catch (JAXBException exception) {
            log.error(exception);
            throw new FileReaderException("Erreur de lecture du fichier TXT", exception);
        }
    }
}

@XmlRootElement(name = "clients")
class XmlNode {
    private List<Client> clients = new ArrayList<>();

    @XmlElement(name = "client")
    public List<Client> getClients() {
        return clients;
    }

    public void setClient(List<Client> clients) {
        this.clients = clients;
    }
}
