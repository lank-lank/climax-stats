package bf.dav.nextep.app.service.reader;

import bf.dav.nextep.app.model.Client;
import bf.dav.nextep.app.service.FileReader;
import bf.dav.nextep.app.service.exception.FileReaderException;
import com.ebay.xcelite.Xcelite;
import com.ebay.xcelite.reader.SheetReader;
import com.ebay.xcelite.sheet.XceliteSheet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class XlsReader implements FileReader {
    private static final Logger log = LogManager.getLogger(XlsReader.class);

    @Override
    public List<Client> getClients(File file) throws Exception {
        List<Client> clients;

        Xcelite xcelite = new Xcelite(file);
        XceliteSheet sheet = xcelite.getSheet(0);
        SheetReader<Client> reader = sheet.getBeanReader(Client.class);

        try {
            clients = new ArrayList<>(reader.read());
            return clients;
        } catch (Exception exception) {
            log.error(exception);
            throw new FileReaderException("Erreur de lecture du fichier EXCEL", exception);
        }
    }
}
