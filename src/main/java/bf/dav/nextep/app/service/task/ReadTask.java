package bf.dav.nextep.app.service.task;

import bf.dav.nextep.app.model.Client;
import bf.dav.nextep.app.service.FileReaderContext;
import javafx.concurrent.Task;

import java.io.File;
import java.util.List;

public class ReadTask extends Task<List<Client>> {
    private final FileReaderContext context;
    private final File file;

    public ReadTask(FileReaderContext context, File file) {
        this.context = context;
        this.file = file;
    }

    @Override
    protected List<Client> call() throws Exception {
        List<Client> clients = context.getClients(file);
        for (int i = 0; i < 100; i++) {
            updateProgress(i, 100);
            Thread.sleep(1);
        }
        return clients;
    }
}
