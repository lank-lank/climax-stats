package bf.dav.nextep.app.service;

import bf.dav.nextep.app.model.Client;

import java.io.File;
import java.util.List;

public class FileReaderContext {
    private final FileReader fileReader;

    public FileReaderContext(FileReader fileReader) {
        this.fileReader = fileReader;
    }

    public List<Client> getClients(File file) throws Exception {
        return this.fileReader.getClients(file);
    }
}
