package bf.dav.nextep.app.service.reader;

import bf.dav.nextep.app.model.Client;
import bf.dav.nextep.app.service.FileReader;
import bf.dav.nextep.app.service.exception.FileReaderException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class JsonReader implements FileReader {
    private static final Logger log = LogManager.getLogger(JsonReader.class);

    @Override
    public List<Client> getClients(File file) throws Exception {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(file, new TypeReference<>() {});
        } catch (IOException exception) {
            log.error(exception);
            throw new FileReaderException("Erreur de lecture du fichier JSON", exception);
        }
    }
}
