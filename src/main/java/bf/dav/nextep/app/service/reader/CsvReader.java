package bf.dav.nextep.app.service.reader;

import bf.dav.nextep.app.model.Client;
import bf.dav.nextep.app.service.FileReader;
import bf.dav.nextep.app.service.exception.FileReaderException;
import com.univocity.parsers.common.processor.BeanListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class CsvReader implements FileReader {
    private static final Logger log = LogManager.getLogger(CsvReader.class);

    @Override
    public List<Client> getClients(File file) throws Exception {
        List<Client> clients;
        try (InputStream inputStream = new FileInputStream(file)) {
            clients = this.parser(inputStream);
            return clients;
        } catch (IOException exception) {
            log.error(exception);
            throw new FileReaderException("Erreur de lecture du fichier CSV", exception);
        }
    }

    private List<Client> parser(InputStream inputStream) {
        List<Client> clients = null;
        if (inputStream != null) {
            BeanListProcessor<Client> rowProcessor = new BeanListProcessor<>(Client.class);
            CsvParser parser = setParserSettings(rowProcessor);
            parser.parse(inputStream, "UTF-8");

            clients = rowProcessor.getBeans();
        }
        return clients;
    }

    private CsvParser setParserSettings(BeanListProcessor<?> rowProcessor) {
        CsvParserSettings parserSettings = new CsvParserSettings();
        parserSettings.setProcessor(rowProcessor);
        parserSettings.setHeaderExtractionEnabled(true);
        parserSettings.setAutoConfigurationEnabled(true);
        parserSettings.detectFormatAutomatically(';', ',');
        parserSettings.setProcessorErrorHandler((e, objects, context) ->
                log.error("Erreur dans le fichier sur la colonne : {}", e.getColumnName())
        );
        return new CsvParser(parserSettings);
    }
}
