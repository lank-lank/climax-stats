package bf.dav.nextep.app.service;

import bf.dav.nextep.app.model.Client;

import java.io.File;
import java.util.List;

public interface FileReader {
    List<Client> getClients(File file) throws Exception;
}
