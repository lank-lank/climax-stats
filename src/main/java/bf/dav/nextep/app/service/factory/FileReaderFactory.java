package bf.dav.nextep.app.service.factory;

import bf.dav.nextep.app.service.FileReader;
import bf.dav.nextep.app.service.reader.*;

import java.util.HashMap;
import java.util.Map;

public class FileReaderFactory {
    private static final Map<String, FileReader> FILE_READERS = new HashMap<>();

    static {
        FILE_READERS.put("xlsx", new XlsReader());
        FILE_READERS.put("xls", new XlsReader());
        FILE_READERS.put("csv", new CsvReader());
        FILE_READERS.put("txt", new TxtReader());
        FILE_READERS.put("json", new JsonReader());
        FILE_READERS.put("xml", new XmlReader());
        FILE_READERS.put("yml", new YamlReader());
        FILE_READERS.put("yaml", new YamlReader());
    }

    public static FileReader getFileReader(String extension) {
        return FILE_READERS.getOrDefault(extension.toLowerCase(), null);
    }
}
