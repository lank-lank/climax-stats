package bf.dav.nextep.app.service.exception;

public class FileReaderException extends Exception {
    public FileReaderException(String message, Throwable cause) {
        super(message, cause);
    }
}
