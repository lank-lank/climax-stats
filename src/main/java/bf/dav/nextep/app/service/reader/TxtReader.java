package bf.dav.nextep.app.service.reader;

import bf.dav.nextep.app.model.Client;
import bf.dav.nextep.app.service.FileReader;
import bf.dav.nextep.app.service.exception.FileReaderException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TxtReader implements FileReader {
    private static final Logger log = LogManager.getLogger(TxtReader.class);

    @Override
    public List<Client> getClients(File file) throws Exception {
        List<Client> clients = new ArrayList<>();

        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] words = line.split("[\\s,;\t]+");

                if (words.length >= 5) {
                    String nom = words[0].trim();
                    String prenom = words[1].trim();
                    Integer age = Integer.parseInt(words[2].replaceAll("[^0-9.\\-]", ""));
                    String profession = words[3].trim();
                    Double salaire = Double.parseDouble(words[4].replaceAll("[^0-9.\\-]", ""));

                    Client client = new Client(nom, prenom, age, profession, salaire);
                    clients.add(client);
                }
            }
            return clients;
        } catch (IOException exception) {
            log.error(exception);
            throw new FileReaderException("Erreur de lecture du fichier TXT", exception);
        }

    }
}
