package bf.dav.nextep.app.utils;

import bf.dav.nextep.app.notification.NotificationType;
import bf.dav.nextep.app.notification.TrayNotification;
import javafx.util.Duration;

public class Toast {
    private static final TrayNotification tray = new TrayNotification();
    private static NotificationType notification = null;

    public static void success(String title, String message) {
        notification = NotificationType.SUCCESS;
        tray.setTitle(title);
        tray.setMessage(message);
        tray.setNotificationType(notification);
        tray.showAndDismiss(Duration.millis(3000));
    }

    public static void error(String title, String message) {
        notification = NotificationType.ERROR;
        tray.setTitle(title);
        tray.setMessage(message);
        tray.setNotificationType(notification);
        tray.showAndDismiss(Duration.millis(3000));
    }

    public static void warning(String title, String message) {
        notification = NotificationType.WARNING;
        tray.setTitle(title);
        tray.setMessage(message);
        tray.setNotificationType(notification);
        tray.showAndDismiss(Duration.millis(3000));
    }
}
