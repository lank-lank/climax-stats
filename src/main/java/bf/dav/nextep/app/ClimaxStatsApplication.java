package bf.dav.nextep.app;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class ClimaxStatsApplication extends Application {
    public static ObservableList<String> stylesheets;
    public static Stage stage;


    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) throws IOException {
        StackPane root = FXMLLoader.load(Objects.requireNonNull(
                ClimaxStatsApplication.class.getResource("/ui/main_frame.fxml"))
        );
        StackPane content = new StackPane();
        content.getChildren().addAll(root);
        Scene scene = new Scene(content);

        stylesheets = scene.getStylesheets();
        stylesheets.addAll(
                Objects.requireNonNull(getClass().getResource("/css/fonts.css")).toExternalForm(),
                Objects.requireNonNull(getClass().getResource("/css/material-color.css")).toExternalForm()
        );

        stage.setScene(scene);
        stage.setMaximized(true);
        ClimaxStatsApplication.stage = stage;
        stage.show();
    }

    @Override
    public void stop() {
        Platform.exit();
    }
}
