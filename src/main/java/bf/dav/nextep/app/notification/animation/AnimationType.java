package bf.dav.nextep.app.notification.animation;

public enum AnimationType {
    FADE,
    SLIDE,
    POPUP;

    AnimationType() {
    }
}
