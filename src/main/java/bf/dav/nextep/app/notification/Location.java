package bf.dav.nextep.app.notification;

public class Location {
    private double x;
    private double y;

    public Location(double xLoc, double yLoc) {
        this.x = xLoc;
        this.y = yLoc;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }
}
