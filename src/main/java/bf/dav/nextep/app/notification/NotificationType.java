package bf.dav.nextep.app.notification;

public enum NotificationType {
    INFORMATION,
    NOTICE,
    SUCCESS,
    WARNING,
    ERROR,
    CUSTOM;

    NotificationType() {
    }
}
