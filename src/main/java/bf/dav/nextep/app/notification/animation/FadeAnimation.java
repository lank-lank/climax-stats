package bf.dav.nextep.app.notification.animation;

import bf.dav.nextep.app.notification.CustomStage;
import javafx.animation.*;
import javafx.util.Duration;


public class FadeAnimation implements TrayAnimation {
    private final Timeline showAnimation;
    private final Timeline dismissAnimation;
    private final SequentialTransition sq;
    private final CustomStage stage;
    private boolean trayIsShowing;

    public FadeAnimation(CustomStage customStage) {
        this.stage = customStage;
        this.showAnimation = this.setupShowAnimation();
        this.dismissAnimation = this.setupDismissAnimation();
        this.sq = new SequentialTransition(this.setupShowAnimation(), this.setupDismissAnimation());
    }

    private Timeline setupShowAnimation() {
        Timeline tl = new Timeline();
        KeyValue kvOpacity = new KeyValue(this.stage.opacityProperty(), 0.0);
        KeyFrame frame1 = new KeyFrame(Duration.ZERO, kvOpacity);
        KeyValue kvOpacity2 = new KeyValue(this.stage.opacityProperty(), 1.0);
        KeyFrame frame2 = new KeyFrame(Duration.millis(3000.0), kvOpacity2);
        tl.getKeyFrames().addAll(frame1, frame2);
        tl.setOnFinished((e) -> {
            this.trayIsShowing = true;
        });
        return tl;
    }

    private Timeline setupDismissAnimation() {
        Timeline tl = new Timeline();
        KeyValue kv1 = new KeyValue(this.stage.opacityProperty(), 0.0);
        KeyFrame kf1 = new KeyFrame(Duration.millis(2000.0), kv1);
        tl.getKeyFrames().addAll(kf1);
        tl.setOnFinished((e) -> {
            this.trayIsShowing = false;
            this.stage.close();
            this.stage.setLocation(this.stage.getBottomRight());
        });
        return tl;
    }

    public AnimationType getAnimationType() {
        return AnimationType.FADE;
    }

    public void playSequential(Duration dismissDelay) {
        ((Animation)this.sq.getChildren().get(1)).setDelay(dismissDelay);
        this.sq.play();
    }

    public void playShowAnimation() {
        this.showAnimation.play();
    }

    public void playDismissAnimation() {
        this.dismissAnimation.play();
    }

    public boolean isShowing() {
        return this.trayIsShowing;
    }
}
