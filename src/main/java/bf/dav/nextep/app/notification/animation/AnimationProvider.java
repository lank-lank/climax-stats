package bf.dav.nextep.app.notification.animation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class AnimationProvider {
    private final List<TrayAnimation> animationsList = new ArrayList<>();

    public AnimationProvider(TrayAnimation... animations) {
        Collections.addAll(this.animationsList, animations);
    }

    public void addAll(TrayAnimation... animations) {
        Collections.addAll(this.animationsList, animations);
    }

    public TrayAnimation get(int index) {
        return this.animationsList.get(index);
    }

    public TrayAnimation findFirstWhere(Predicate<? super TrayAnimation> predicate) {
        return this.animationsList.stream().filter(predicate).findFirst().orElse(null);
    }

    public List<TrayAnimation> where(Predicate<? super TrayAnimation> predicate) {
        return this.animationsList.stream().filter(predicate).collect(Collectors.toList());
    }
}
